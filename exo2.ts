interface Administrateur {
  nom: string;
  email: string;
  ip: string;
  dt_connexion: Date;
  login: string;
  password: string;
}
// interface UtilisateurAnonyme {
//   nom?: string;
//   ip: string;
// }

type UtilisateurAnonyme = Partial<Pick<Administrateur , "nom" | "ip">> ;
