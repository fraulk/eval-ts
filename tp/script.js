"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
let data;
document.addEventListener("DOMContentLoaded", () => __awaiter(void 0, void 0, void 0, function* () {
    fetch("https://jsonplaceholder.typicode.com/users")
        .then((response) => response.json())
        .then((json) => {
        console.log(json);
        let userPost = [];
        fetch("https://jsonplaceholder.typicode.com/posts")
            .then((responsePosts) => responsePosts.json())
            .then((posts) => {
            console.log(posts);
            json.map((item, i) => {
                // console.log(item);
                userPost.push({
                    name: item.name,
                    posts: posts.filter((post) => post.userId == item.id),
                });
                data = userPost;
                render(userPost);
            });
            console.log(userPost);
        });
    });
}));
document.querySelector("form").addEventListener("submit", (e) => {
    e.preventDefault();
    const input = document.querySelector("input[type='text']");
    const userFilter = data.filter((user) => user.name
        .toLowerCase()
        .includes(input.value.toLowerCase()));
    render(userFilter);
});
const render = (userPost) => {
    let html = "<ul>";
    userPost.forEach((user) => (html += `<li>${user.name}, Number of Post : ${user.posts.length}</li>`));
    html += "</ul>";
    document.querySelector("#userList").innerHTML = html;
};
