let data: any;

document.addEventListener("DOMContentLoaded", async () => {
  fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => response.json())
    .then((json) => {
      console.log(json);
      let userPost: any[] = [];

      fetch("https://jsonplaceholder.typicode.com/posts")
        .then((responsePosts) => responsePosts.json())
        .then((posts) => {
          console.log(posts);

          json.map((item: any, i: number) => {
            // console.log(item);
            userPost.push({
              name: item.name,
              posts: posts.filter((post) => post.userId == item.id),
            });
            data = userPost
            render(userPost)
          });
          console.log(userPost);
        });
    });
});

document.querySelector("form").addEventListener("submit", (e: Event) => {
  e.preventDefault();
  const input = document.querySelector(
    "input[type='text']"
  ) as HTMLInputElement;
  
  const userFilter = data.filter((user) =>
    user.name
      .toLowerCase()
      .includes(input.value.toLowerCase())
  );
  render(userFilter)
});

const render = (userPost: any) => {
    let html = "<ul>";
    userPost.forEach(
      (user: any) =>
        (html += `<li>${user.name}, Number of Post : ${user.posts.length}</li>`)
    );
    html += "</ul>";
    document.querySelector("#userList").innerHTML = html;
}